import express from "express";
import logger from "morgan";
import swaggerUi from "swagger-ui-express";
import Passport from "passport";
import bodyParser from "body-parser";
import swaggerDocuments from "./config/swagger.json";
import { connect } from "./config/db";
import { restRouter } from "./api";
import { configJWTStrategy } from "./api/middlewares/passport-jwt.js";
import _ from "lodash";
const cors = require("cors");

const app = express();
app.use(cors());
const PORT = 3000;
const hostname = '0.0.0.0'
const server = require("http").createServer(app);
const io = require("socket.io").listen(server);

// connect();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/uploads", express.static("uploads"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  req.header(
    "Access-Control-Allow-Methods",
    "PUT, POST, PATCH, DELETE, GET, OPTIONS"
  );
  if (req.method === "OPTIONS") {
    res.send(200);
  } else {
    next();
  }
});

const { User } = require("./api/helpers/UserClass");

require("./api/socket/streams")(io, User, _);
// require("./api/socket/private")(io);
app.use(logger("dev"));
app.use(Passport.initialize());
configJWTStrategy();
app.use("/api", restRouter);
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocuments, {
    explorer: true
  })
);
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.message = "Invalid route";
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.json({
    error: {
      message: error.message
    }
  });
});

server.listen(process.env.PORT || PORT, () => {
  console.log(`Server is running at PORT http://${hostname}:${PORT}`);
});
