import { USER_ROLE } from "../resources/user/user.model";

export const isUser = (req, res, next) => {
    if(req.user.role !== USER_ROLE) {
        return res.json({err: "YOU ARE NOT AUTHORIZED FOR THIS"});
    }
    next();
}
