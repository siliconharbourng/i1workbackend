import express from 'express';
import { profileRouter } from './resources/profile';
import { userRouter } from './resources/user';
import { bookingRouter } from './resources/booking';
import { workerLocationRouter } from './resources/worker-location';
import { reviewRouter } from './resources/reviews';
import { sponsorRouter } from './resources/sponsor';
import { messageRouter } from './resources/message';

export const restRouter = express.Router();

restRouter.use('/profiles', profileRouter);
restRouter.use('/users', userRouter);
restRouter.use('/booking', bookingRouter);
restRouter.use('/workers-location', workerLocationRouter);
restRouter.use('/reviews', reviewRouter);
restRouter.use('/sponsor', sponsorRouter);
restRouter.use('/message', messageRouter)