module.exports = function(io, User, _) {
  const userData = new User();

  io.on("connection", socket => {
    socket.on("refresh", msg => {
      console.log(msg);
      
      io.emit("refreshpage", {});
    });

    socket.on("online", data => {
      socket.join(data.room);
      userData.EnterRoom(socket.id, data.user, data.room);
      const list = userData.GetList(data.room);
      io.emit('usersOnline', list)      
    })

    socket.on('disconnect', () => {
      const user = userData.RemoveUser(socket.id)
      if (user) {
        const userObj = userData.GetList(user.room)
        io.emit('usersOnline', userObj)
      }
    })
  });
};
