import Joi from "joi";
import bcrypt from "bcryptjs";

export default {
  encryptPassword(plaintext) {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(plaintext, salt);
  },
  comparePassword(plaintext, encryptPassword) {
    return bcrypt.compareSync(plaintext, encryptPassword);
  },
  validateSignup(body) {
    const schema = Joi.object().keys({
      first_name: Joi.string().required(),
      last_name: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      phone_no: Joi.number()
        .integer()
        .required(),
      password: Joi.string().required(),
      refered_by: Joi.string().allow(""),
      address: Joi.string().required(),
      role: Joi.string().required(),
      accept_discliamer: Joi.boolean().required()
    });
    const { value, error } = Joi.validate(body, schema);
    if (error && error.details) {
      return res.status(500).json({ message: error.details });
    }
    return { value };
  },
  validateLogin(body) {
    const schema = Joi.object().keys({
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string().required()
    });
    const { value, error } = Joi.validate(body, schema);
    if (error && error.details) {
      return res.status(500).json({ message: error.details });
    }
    return { value };
  }
};
