import express from "express";
import passport from "passport";
import userController from "./user.controller";
import multer from "multer";

// const storage = multer.diskStorage({
//   destination: function(req, file, cb) {
//     cb(null, "./uploads/");
//   },
//   filename: function(req, file, cb) {
//     cb(null, file.originalname);
//   }
// });

// const fileFilter = (req, file, cb) => {
//   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };

// const upload = multer({
//   storage: storage,
//   limits: { fileSize: 1024 * 1024 * 5 },
//   fileFilter: fileFilter
// });

export const userRouter = express.Router();
userRouter.post("/signup", userController.signup);
userRouter.post("/login", userController.login);
userRouter.get(
  "/me",
  passport.authenticate("jwt", { session: false }),
  userController.authenticate
);
userRouter.get("/", userController.findAll);
userRouter.get("/all-users/:id", userController.findOneId)
userRouter.get("/:slug", userController.findOne);
userRouter.put("/update-profile/:id", userController.update);
userRouter.patch("/reg/:id", userController.regComplete);
userRouter.put("/location/:id", userController.currentLocation);
userRouter.get("/subcribed/workers", userController.findworkers)
