import mongoose from "mongoose";
import Slug from "mongoose-slug-generator";
import shortid from "shortid";

mongoose.plugin(Slug);
shortid.generate();
shortid.characters(
  "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!"
);

export const ADMIN_ROLE = "Admin";
export const USER_ROLE = "Clients";
export const WORKER_ROLE = "Worker";
const { Schema } = mongoose;
const userSchema = new Schema({
  first_name: { type: String, required: "true" },
  last_name: { type: String, required: "true" },
  accept_discliamer: { type: Boolean, required: "true" },
  slug: { type: String, slug: ["first_name", "last_name"], unique: true },
  referal_link: { type: String, default: shortid.generate, unique: true },
  refered: [{ invited: { type: mongoose.Schema.Types.ObjectId, ref: "User" } }],
  refered_by: { type: String },
  avatar: { type: String, default: "" },
  email: { type: String, required: "true", unique: "true" },
  phone_no: { type: Number, required: "true", unique: "true" },
  password: { type: String, required: "true" },
  role: { type: String, required: "true" },
  jobs: [{ job: { type: mongoose.Schema.Types.ObjectId, ref: "Booking" } }],
  job_offers: [
    { offer: { type: mongoose.Schema.Types.ObjectId, ref: "Booking" } }
  ],
  review: { type: Number },
  reviews: [
    {
      reviewId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      rating: { type: Number, default: "" },
      comment: { type: String, default: "" },
      created: { type: Date, default: Date.now() }
    }
  ],
  primary_skill: { type: String, default: "" },
  secondary_skill: { type: String, default: "" },
  current_location: { 
    type: {type: String, enum: ['Point'] },
    coordinates: { type:[Number], required: true }
   },
  skill_level: { type: String, default: "" },
  sporsors_to: [
    { usersponsored: { type: mongoose.Schema.Types.ObjectId, ref: "User" } }
  ],
  sponsored_by: [
    { sponsor: { type: mongoose.Schema.Types.ObjectId, ref: "User" } }
  ],
  address: { type: String, default: "" },
  notifications: [
    {
      senderId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      message: { type: String, default: "" },
      viewProfile: { type: Boolean, default: false },
      created: { type: Date, default: Date.now() },
      read: { type: Boolean, default: false },
      date: { type: String, default: "" }
    }
  ],
  chatList: [
    {
      receiverId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      msgId: { type: mongoose.Schema.Types.ObjectId, ref: "Message" }
    }
  ],
  created_date: { type: String, default: Date.now() }
});

export default mongoose.model("User", userSchema);
