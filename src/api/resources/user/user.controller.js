import userService from "./user.service";
import User from "./user.model";
import jwt from "../../helpers/jwt";

export default {
  async signup(req, res) {
    try {
      const { value, error } = userService.validateSignup(req.body);
      if (error) {
        return res.status(400).json({ message: error });
      }
      const user = await User.findOne({ email: value.email });
      if (user) {
        return res.status(401).json({ message: "Email already exist" });
      }
      const phone = await User.findOne({ phone_no: value.phone_no });
      if (phone) {
        return res.status(401).json({ message: "Phone number already exist" });
      }
      const accept_discliamer = value.accept_discliamer == true;
      if (!accept_discliamer) {
        return res.status(401).json({ message: "Please accept Term of use" });
      }
      const ref = value.refered_by == "";
      const referal =
        ref || (await User.findOne({ refered_link: value.refered_by }));
      if (!referal) {
        return res.status(401).json({ message: "Referal Code doesn't exist" });
      }

      const ecryptedPass = userService.encryptPassword(value.password);
      const body = {
        email: value.email,
        first_name: value.first_name,
        last_name: value.last_name,
        phone_no: value.phone_no,
        password: ecryptedPass,
        role: value.role,
        refered_by: value.refered_by,
        address: value.address,
        accept_discliamer: value.accept_discliamer,
        created_date: new Date()
      };
      User.create(body).then(user => {
        User.update(
          {
            referal_link: value.refered_by
          },
          {
            $push: {
              refered: {
                invited: user._id
              }
            }
          }
        );
        const token = jwt.issue({ user }, "1d");
        return res.status(200).json({ success: true, token, user });
      });
    } catch (err) {
      console.error(err);
      return res
        .status(500)
        .send({ message: "Please make sure you complete your informations" });
    }
  },
  async login(req, res) {
    try {
      const { value, error } = userService.validateLogin(req.body);
      if (error) {
        return res.status(400).json({ message: error });
      }
      const email = value.email == "";
      if (email) {
        return res.status(401).json({ message: "Email must not be empty" });
      }
      const user = await User.findOne({ email: value.email });
      if (!user) {
        return res.status(401).json({ message: "Email Does not Exist" });
      }
      const password = value.password == "";
      if (password) {
        return res.status(401).json({ message: "Password Must not be empty" });
      }
      const authenticated = userService.comparePassword(
        value.password,
        user.password
      );
      if (!authenticated) {
        return res.status(401).json({ message: "Incorrect Password" });
      }
      // return res.json(user);
      const token = jwt.issue({ user }, "1d");
      return res.json({ token, user });
    } catch (err) {
      console.error(err);
      return res.status(500).send({
        message: "Please make sure you Insert your EMAIL and PASSWORD"
      });
    }
  },
  authenticate(req, res) {
    return res.json(req.user);
  },

  async findAll(req, res) {
    try {
      const user = await User.find()
        .populate("jobs.job")
        .populate(
          "refered.invited",
          "_id first_name last_name avatar email phone_no"
        )
        .populate("current_location.locationId");
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send(err);
    }
  },

  async findworkers(req, res) {
    try {
      const user = await User.find({
        role: "Worker",
        //   current_location : {
        // 	$near:{
        // 		$geometry:{
        // 			type:"Point",
        // 			coordinates: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)]
        // 		},
        // 		$maxDistance:10000
        //   }, function(err, location) {
        //     if(err) {
        //       res.send(err)
        //     } else {
        //       res.send(location)
        //     }
        //   }
        // }
      })
        .populate("jobs.job")
        .populate(
          "refered.invited",
          "_id first_name last_name avatar email phone_no"
        )
        .populate("current_location.locationId");
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send(err);
    }
  },
  

  
  async findOne(req, res) {
    try {
      const { slug } = req.params;
      const user = await User.findOne({ slug: slug });
      if (!user) {
        return res.status(404).json({ message: "Could not Find Profile" });
      }
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: "User doesn't exist" });
    }
  },

  async findOneId(req, res) {
    try {
      const { id } = req.params;
      const user = await User.findOne({ _id: id });
      if (!user) {
        return res.status(404).json({ message: "Could not Find Profile" });
      }
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: "User doesn't exist" });
    }
  },

  async update(req, res) {
    try {
      const { id } = req.params;
      const body = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        password: req.body.password,
        primary_skill: req.body.primary_skill,
        secondary_skill: req.body.secondary_skill,
        skill_level: req.body.skill_level,
        address: req.body.address
      };
      const user = await User.findOneAndUpdate({ _id: id }, body, {
        new: true
      });
      if (!user) {
        return res.status(404).json({ message: "User Does not exist" });
      }
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: "Update was not successfull" });
    }
  },

  async currentLocation(req, res) {
    try {
      const { id } = req.params;
      const body = {
        current_location: {
          type: req.body.type,
          coordinates: req.body.coordinates
        }
      };
      const user = await User.findOneAndUpdate({ _id: id }, body, {
        new: true
      });
      if (!user) {
        return res.status(404).json({ message: "User Does not exist" });
      }
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: "Update was not successfull" });
    }
  },

  async regComplete(req, res) {
    try {
      const { id } = req.params;
      const body = {
        primary_skill: req.body.primary_skill,
        secondary_skill: req.body.secondary_skill,
      };
      const user = await User.findByIdAndUpdate({ _id: id }, body, {
        new: true
      });
      if (!user) {
        return res.status(404).json({ message: "User Does not exist" });
      }
      const primary = req.body.primary_skill == ""
      if (primary) {
        return res.status(401).json({ message: "Let Us Know your Main skill to enable you get a job" });
      }
      return res.json(user);
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: "Update was not successfull" });
    }
  }
};
