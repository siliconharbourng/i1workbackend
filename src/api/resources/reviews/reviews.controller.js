import Joi from 'joi';
import Review from './reviews.model';
import User from '../user/user.model';


export default{
    async create (req, res) {
        try {
        const schema = Joi.object().keys({
            rating: Joi.number(),
            comment: Joi.string()
        });
        const {value, error} = Joi.validate(req.body, schema);
        if(error && error.details){
            return res.status(400).json({ message: 'An Error Occured', error });
        }
        const body = {
            user: req.user._id,
            rating: req.body.rating,
            comment: req.body.comment,
            createdAt: new Date()
        }
        Review.create(body)
        .then( async (review) => {
            await User.update({
                _id: req.body._id
            }, {
                $push: {reviews: {
                    reviewId: req.body._id,
                }}
            })
            return res.status(200).json(review); 
        });
        } catch(err) {
            console.error(err);
            return res.status(500).json({ message: 'An Error Occured', err });
        }
    },

    async findAll(req, res) {
        try {
            const reviews = await Review.find();
            return res.json(reviews);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'An Error Occured', err });
        }
    },

    async findOne(req, res) {
        try {
            const { id } = req.params;
            const review = await Review.findById(id).populate('user');
            if(!review) {
                return res.status(404).json({ message: 'No Review History' })
            }
            return res.json(review);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'An Error Occured', err });
        }
    },

    async delete(req,res) {
        try {
            const { id } = req.params;
            const review = await Review.findOneAndDelete({_id: id});
            if(!review) {
                return res.status(404).json({ message: 'No Review History' })
            }
            return res.json({ message: 'Review Deleted' });
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'An Error Occured', err });
        }
    },

    async update(req,res) {
        try {
            const { id } = req.params;
            const schema = Joi.object().keys({
                    rating: Joi.number(),
                    comment: Joi.string()
            });
            const {value, error} = Joi.validate(req.body, schema);
            if(error && error.details){
                return res.status(400).json(error)
            }
            const review = await Review.findOneAndUpdate({_id: id },value,{new:true});
            if(!review) {
                return res.status(404).json({ message: 'Could not Find review' })
            }
            return res.json(review);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'An Error Occured', err });
        }
    },
}