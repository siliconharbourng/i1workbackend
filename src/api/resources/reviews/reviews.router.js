import express from "express";
import passport from "passport";
import { isAdmin } from "../../middlewares/is-Admin";
import { isUser } from "../../middlewares/is-User";
import reviewsController from "./reviews.controller";


export const reviewRouter = express.Router();
const admin_right =  [passport.authenticate('jwt', {session:false}), isAdmin];
const user_right = [passport.authenticate('jwt', {session:false}), isUser];

reviewRouter
.route('/')
.get(admin_right, reviewsController.findAll)
.post(admin_right, reviewsController.create)
.post(reviewsController.create);

reviewRouter
.route('/:id')
.get(admin_right, reviewsController.findOne)
.get(user_right, reviewsController.findOne)
.delete(admin_right, reviewsController.delete)
.delete(user_right, reviewsController.delete)
.put(passport.authenticate('jwt', {session:false}), reviewsController.update);