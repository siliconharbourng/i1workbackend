import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const { Schema } = mongoose;
const reviewSchema = new Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    rating: { type: Number, default: '' },
    comment: { type: String, default: '' },
    createdAt: { type: String, default: Date.now() }
    
});
reviewSchema.plugin(mongoosePaginate);

export default mongoose.model('Review', reviewSchema)