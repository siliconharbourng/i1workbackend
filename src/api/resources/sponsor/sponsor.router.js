import express from 'express';
import passport from 'passport';
import sponsorController from './sponsor.controller';

export const sponsorRouter = express.Router();
sponsorRouter.post('/', sponsorController.sponsor);
