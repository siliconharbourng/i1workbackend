import User from '../user/user.model';

export default {
    async sponsor (req, res) {
       const sponsor = async () => {
            await User.update({
                _id: req.user._id,
                'sponsors_to.usersponsored' : { $ne: req.body.usersponsored }
            }, {
                $push: {
                    sponsors_to: {
                        usersponsored: req.body.usersponsored
                    }
                }
            });

            await User.update({
                _id: req.body.usersponsored,
                'sponsored_by.sponsor' : { $ne: req.user._id }
            }, {
                $push: {
                    sponsored_by: {
                        sponsor: req.user._id
                    },
                    notifications: {
                        senderId: req.user._id,
                        message: `${req.user.first_name} ${res.user.last_name} is now your Sponsor.`,
                        created: new Date()
                    }
                }
            })
       };

       sponsor()
        .then( () => {
            res.status(200).json({ message: 'Sponsoring User now' })
        })
        .catch( (err) => {
            console.error(err);
            return res.status(500).json({ message: 'An error occured'});
        });
    }

}