import express from 'express';
import passport from 'passport';
import profileController from './profile.controller';
import { isAdmin } from '../../middlewares/is-Admin';

export const profileRouter = express.Router();

profileRouter
.route('/')
.post([passport.authenticate('jwt', {session:false}), isAdmin], profileController.create)
.get([passport.authenticate('jwt', {session:false}), isAdmin], profileController.findAll);

profileRouter
.route('/:id')
.get(passport.authenticate('jwt', {session:false}), profileController.findOne)
.delete([passport.authenticate('jwt', {session:false}), isAdmin], profileController.delete)
.put(passport.authenticate('jwt', {session:false}), profileController.update);