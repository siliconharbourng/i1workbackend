import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const { Schema } = mongoose;
const profileSchema = new Schema({
    first_name: {
        type: String,
        required: [true, 'You Must give your First Name']
    },
    last_name: {
        type: String,
        required: [true, 'You Must give your Last Name']
    },
});
profileSchema.plugin(mongoosePaginate);

export default mongoose.model('Profile', profileSchema)