import Joi from "joi";
import Profile from "./profile.model";

export default {
    async create(req,res){
        console.log(req.body);
        try {
            const schema = Joi.object().keys({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
        });
        const {value, error} = Joi.validate(req.body, schema);
        if(error && error.details){
            return res.status(400).json(error)
        }
        const profile = await Profile.create(Object.assign({},value,{owner: req.user._id}));
        return res.json(profile);
        } catch(err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },

    async findAll(req, res) {
        try {
            const profile = await Profile.paginate();
            return res.json(profile);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },

    async findOne(req, res) {
        try {
            const { id } = req.params;
            const profile = await Profile.findById(id);
            if(!profile) {
                return res.status(404).json({ message: 'Could not Find Profile' })
            }
            return res.json(profile);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },

    async delete(req,res) {
        try {
            const { id } = req.params;
            const profile = await Profile.findOneAndDelete({_id: id});
            if(!profile) {
                return res.status(404).json({ message: 'Could not Find Profile' })
            }
            return res.json({ message: 'Profile Deleted' });
        } catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },

    async update(req,res) {
        try {
            const { id } = req.params;
            const schema = Joi.object().keys({
                first_name: Joi.string().required(),
                last_name: Joi.string().required(),
            });
            const {value, error} = Joi.validate(req.body, schema);
            if(error && error.details){
                return res.status(400).json(error)
            }
            const profile = await Profile.findOneAndUpdate({_id: id },value,{new:true});
            if(!profile) {
                return res.status(404).json({ message: 'Could not Find Profile' })
            }
            return res.json(profile);
        } catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
}