import express from "express";
import passport from "passport";
import { isAdmin } from "../../middlewares/is-Admin";
import bookingController from "./booking.controller";
import { isUser } from "../../middlewares/is-User";


export const bookingRouter = express.Router();
const admin_right =  [passport.authenticate('jwt', {session:false}), isAdmin];
const user_right = [passport.authenticate('jwt', {session:false}), isUser];

bookingRouter.get('/worker-job-status', bookingController.WorkerJobStatus);
bookingRouter.get('/:id/owner-job-status', bookingController.OwnerJobStatus);
bookingRouter.get('/:id/work-job-status', bookingController.workJobStatus);
bookingRouter
.route('/')
.get(bookingController.findAll)
.post(bookingController.create)
// .post(user_right, bookingController.create);

bookingRouter
.route('/status/:id')
.put(bookingController.JobStatusUpdate)
.patch(bookingController.JobStatusCancel)
.get(bookingController.findAcceptedJobs)

bookingRouter
.route('/worker/accepted/:id')
.get(bookingController.findReceivedJobs)

bookingRouter
.route('/request/:id')
.get(bookingController.findAllNewRequest)// for owners available request

bookingRouter
.route('/:id')
.get(bookingController.findAllClients)

bookingRouter
.route('/jobsrequest/:worker')
.get(bookingController.findAllNewJobs)// for workers available jobs

bookingRouter
.route('/:id/booked/:Clientid')
.get(bookingController.findClients)
// .get(user_right, bookingController.findOne)
// .delete(admin_right, bookingController.delete)
// .delete(user_right, bookingController.delete)
// .put(passport.authenticate('jwt', {session:false}), bookingController.update);