import Joi from 'joi';
import Booking from './booking.model';
import User from '../user/user.model';


export default{
    async create(req, res) {
        try {
            const schema = Joi.object().keys({
                work_location: Joi.object({
                    type: Joi.string().required(),
                    coordinates: Joi.array().required()
                }).required(),
                worker: Joi.string().required(),
                owner: Joi.string().required(),
                status: Joi.string().optional()
            });
            const {value, error} = Joi.validate(req.body, schema);
            if (error && error.details) {
                return res.status(400).json(error);
            }
            const user = await User.findById({ _id: value.worker }) && await User.findOne({ _id: value.owner });
            if (!user) {
                return res.status(401).json({ message: "User Code doesn't exist" });
            }
            const pending = await Booking.find({owner: value.owner, status: "PENDING" })
            const accepted = await Booking.find({ owner: value.owner, status: "ACCEPTED" })
            
            if (pending.length > 0) {
                return res.status(401).json({ message: "You currently need to cancel your current job"}) 
            }
            
            if (accepted.length > 0) {
                return res.status(401).json({ message: "You currently need to cancel your current job"})
            }
            
            const job = await Booking.create(value);
            return res.json(job);
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
        
    },

    async findAll(req, res) {
        try {
            const booking = await Booking.find()
            .populate("owner")
            .populate("worker");
            return res.json(booking);
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findClients(req, res) {
        try {
            const { id } = req.params;
            const owned = {
                owner: req.body.owner
            }
            const booking = await Booking.findById({_id: id, owned})
            .populate("owner")
            .populate("worker");
            return res.json(booking);
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findAllClients(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ owner: id })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findAllNewJobs(req, res) {
        try {
            const { worker } = req.params;
            const booking = await Booking.find({ worker: worker, isAvailable: true })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findAllNewRequest(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ owner: id, isAvailable: true })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },
    async OwnerJobStatus(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ owner: id })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },
    async workJobStatus(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ worker: id })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },
    async WorkerJobStatus(req, res) {        
        try {
            const { id } = req.params;
            const booking = await Booking.find(req.query)
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findAcceptedJobs(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ owner: id, status: "ACCEPTED" })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async findReceivedJobs(req, res) {
        try {
            const { id } = req.params;
            const booking = await Booking.find({ worker: id, status: "ACCEPTED" })
            .populate("owner")
            .populate("worker");
            return res.json(booking)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async JobStatusUpdate (req, res) {
        try {
            const { id } = req.params;
            const body = {
                status: req.body.status,
            };
            const booking = await Booking.findOneAndUpdate({ _id: id }, body, {
                new: true
            });
            if (!booking) {
                return res.status(404).json({ message: "Job Does not exist" });
            }
            return res.json(booking);
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    },

    async JobStatusCancel (req, res) {
        try {
            const { id } = req.params;
            const body = {
                status: req.body.status,
                isAvailable: false
            };
            const booking = await Booking.findOneAndUpdate({ _id: id }, body, {
                new: true
            });
            if (!booking) {
                return res.status(404).json({ message: "Job Does not exist" });
            }
            return res.json(booking);
        } catch (error) {
            console.log(error)
            return res.status(500).send(error);
        }
    }
}