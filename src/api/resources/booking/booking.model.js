import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const { Schema } = mongoose;
const bookingSchema = new Schema({
    work_location: { 
        type: {type: String, enum: ['Points'] },
        coordinates: { type:[Number], required: true }
       },
    worker: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    isAvailable: { type: Boolean, default: true }, 
    status: { type: String, enum: ['PENDING', 'CANCEL', 'ACCEPTED', 'REJECTED', 'ARRIVED LOCATION', 'INPROGRESS'], default:'PENDING', required: true  },
    createdAt: { type: String, default: Date.now() }
    
});
bookingSchema.plugin(mongoosePaginate);

export default mongoose.model('Booking', bookingSchema)