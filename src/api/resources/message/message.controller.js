import Message from "./message.model";
import Conversation from "../conversation/conversation.model";
import User from "../user/user.model"

export default {
    async GetAllMessages(req, res) {
        const { sender_Id, receiver_Id} = req.params        
        const conversation = await Conversation.findOne({
            $or: [
                {
                    $and: [{ 'participants.senderId': sender_Id }, { 'participants.receiverId': receiver_Id}]
                },
                {
                    $and: [{ 'participants.senderId': receiver_Id }, { 'participants.receiverId': sender_Id}]
                },
            ]
        }).select('_id')

        if (conversation) {
            const messages = await Message.findOne({
                conversationId: conversation._id
            })
            res.status(200).json({message: 'Messages returned', messages})
        } else {
            
        }
    },
    SendMessage(req, res) {        
        const { sender_Id, receiver_Id } = req.params;
        
        Conversation.find({
            $or: [
                { participants: {$elemMatch: {senderId: sender_Id, receiverId: receiver_Id}}},
                { participants: {$elemMatch: {senderId: receiver_Id, receiverId: sender_Id}}}
            ]
        }, async (err, result) => {                       
            if (result.length > 0) { 
                               
                await Message.update({
                    conversationId: result[0]._id
                }, {
                    $push: {
                        messages: {
                            senderId: req.params.sender_Id,
                            receiverId: req.params.receiver_Id,
                            senderFirstName: req.body.senderFirstName,
                            receiverFirstName: req.body.receiverFirstName,
                            message: req.body.message 
                        }
                    }
                })
                .then(() => res.status(200).json({ message: 'Message sent to user' }))
                .catch(err => res.status(500).json({ message: 'Error occured' }))
            } else {
                const newConversation = new Conversation();
                newConversation.participants.push({
                    senderId: req.params.sender_Id,
                    receiverId: req.params.receiver_Id
                });

                const saveConversation = await newConversation.save();
                console.log(saveConversation);
                
                
                const newMessage = new Message();
                newMessage.conversationId = saveConversation._id;
                newMessage.sender = req.body.senderFirstName;
                newMessage.receiver = req.body.receiverFirstName;
                newMessage.messages.push({
                    senderId: req.params.sender_Id,
                    receiverId: req.params.receiver_Id,
                    senderFirstName: req.body.senderFirstName,
                    receiverFirstName: req.body.receiverFirstName,
                    message: req.body.message 
                });

                await User.updateOne({
                    _id: req.params.sender_Id
                }, {
                    $push: {
                        chatList: {
                            $each: [
                                {
                                    receiverId: req.params.receiver_Id,
                                    msgId: newMessage._id
                                }
                            ],
                            $position: 0
                        }
                    }
                });

                await User.updateOne({
                    _id: req.params.receiver_Id
                }, {
                    $push: {
                        chatList: {
                            $each: [
                                {
                                    receiverId: req.params.sender_Id,
                                    msgId: newMessage._id
                                }
                            ],
                            $position: 0
                        }
                    }
                }) 
                console.log(saveConversation, newMessage);
                
                await newMessage.save()
                .then(() => res.status(200).json({message: 'Message Sent'}))
                .catch(err => res.status(500).json({message: 'Error occured'}))
            }
        })
    }
}

