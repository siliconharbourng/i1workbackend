import express from 'express';
import messagecontroller from './message.controller';


export const messageRouter = express.Router();

messageRouter
.route('/chats/:sender_Id/:receiver_Id')
.post(messagecontroller.SendMessage)
.get(messagecontroller.GetAllMessages)