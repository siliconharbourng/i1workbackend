import mongoose from 'mongoose';

const { Schema } = mongoose;
const messageSchema = new Schema({
    conversationId: { type: mongoose.Schema.Types.ObjectId, ref: 'Converstion'},
    sender: { type: String, default: ''},
    receiver: { type: String, default: '' },
    messages: [
        {
            senderId: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            receiverId: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            senderFirstName: { type: String },
            receiverFirstName: { type: String },
            message: { type: String, default: ''},
            isRead: { type: Boolean, default: false},
            createdAt: { type: Date, default: Date.now() }

        }
    ]
});

export default mongoose.model('Message', messageSchema)