import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const { Schema } = mongoose;
const locationSchema = new Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    createdAt: { type: String, default: Date.now() }
});
locationSchema.plugin(mongoosePaginate);

export default mongoose.model('Location', locationSchema)