import Joi from 'joi';
import Location from './workers-locations.model';
import User from '../user/user.model';

export default {
    async create (req, res) {
        try {
        const schema = Joi.object().keys({
            latitude: Joi.number().required(),
            longitude: Joi.number().required()
        });
        const {value, error} = Joi.validate(req.body, schema);
        if(error && error.details){
            return res.status(400).json(error)
        }
        const body = {
            latitude: req.body.latitude,
            longitude: req.body.longitude,
            createdAt: new Date()

        }
        const location = await Location.create(body)
        .then(async (location) => {
            await User.update({
                $push: {current_location: {
                    locationId: location._id,
                    latitude: location.latitude,
                    longitude: location.longitude,
                    created: new Date()
                }}
            })
            return res.status(200).json(location);
        });
        } catch(err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
}