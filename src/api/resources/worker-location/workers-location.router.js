import express from "express";
import workersLocationController from "./workers-location.controller";


export const workerLocationRouter = express.Router(); 

workerLocationRouter
.route('/')
.post(workersLocationController.create);
