import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
export const connect = () => mongoose.connect('mongodb://127.0.0.1:27017/i1work_api', { useNewUrlParser: true, useCreateIndex: true });